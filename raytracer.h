#pragma once

#include <camera_options.h>
#include <fstream>
#include <image.h>
#include <postprocess.h>
#include <ray.h>
#include <render_options.h>
#include <scene.h>
#include <string>

Vec3<double> GetPixelColor(const Scene& scene, int x, int y, const CameraOptions& camera_options,
                           const RenderOptions& render_options) {
    Ray ray = CastRay(x, y, camera_options);
    return GetColor(ray, scene, render_options.depth);
}

Image PostProcess(std::vector<std::vector<Vec3<double>>>* im) {
    ToneMapping(im);
    GammaCorrection(im);
    Image result((*im)[0].size(), im->size());
    for (size_t y = 0; y < im->size(); ++y) {
        for (size_t x = 0; x < (*im)[0].size(); ++x) {
            Vec3<double> final_color = (*im)[y][x] * 255;
            int r = floor(final_color.X()), g = floor(final_color.Y()), b = floor(final_color.Z());
            result.SetPixel({r, g, b}, y, x);
        }
    }
    return result;
}

Image Render(const std::string& filename, const CameraOptions& camera_options,
             const RenderOptions& render_options) {
    Scene scene = ReadSceneFile(filename);
    std::vector<std::vector<Vec3<double>>> result(
        camera_options.screen_height, std::vector<Vec3<double>>(camera_options.screen_width));
    for (int x = 0; x < camera_options.screen_width; ++x) {
        for (int y = 0; y < camera_options.screen_height; ++y) {
            result[y][x] = GetPixelColor(scene, x, y, camera_options, render_options);
        }
    }
    return PostProcess(&result);
}