#pragma once

#include <cctype>
#include <string_utils.h>
#include <vec3.h>
#include <vector>
#include <optional>
#include <ray.h>
#include <unordered_map>

struct Material {
    Vec3<double> ambient;
    Vec3<double> ambient_additional;
    Vec3<double> diffuse;
    Vec3<double> specular;
    double transparency;
    double specular_exponent;
    double refraction_index;
    bool is_illuminating;

    Material()
        : transparency(0), specular_exponent(0), refraction_index(0), is_illuminating(false) {
    }
};

struct GeometricVertex {
    Vec3<double> point;
    double weight;
};

struct TextureCoordinates {
    Vec3<double> point;
};

struct VertexNormal {
    Vec3<double> value;
};

struct Vertex {
    GeometricVertex geom_vertex;
    std::optional<TextureCoordinates> texture_coord;
    std::optional<VertexNormal> vertex_normal;
};

struct Shape {
    std::string material;

    Shape(const std::string& material) : material(material) {
    }
    virtual std::optional<Vec3<double>> Intersection(const Ray&) const = 0;
    virtual Vec3<double> Normal(const Vec3<double>&) const = 0;
};

struct Triangle final : public Shape {
    Vertex a;
    Vertex b;
    Vertex c;

    Triangle(const Vertex& a, const Vertex& b, const Vertex& c, const std::string& material)
        : Shape(material), a(a), b(b), c(c) {
    }

    std::optional<Vec3<double>> Intersection(const Ray& ray) const {
        Vec3 ab = b.geom_vertex.point - a.geom_vertex.point;
        Vec3 ac = c.geom_vertex.point - a.geom_vertex.point;
        Vec3 pvec = ray.Direction().Cross(ac);
        double det = ab.Dot(pvec);
        if (fabs(det) < 1e-8) {  // if det is close to 0, then ray and triangle are parallel
            return {};
        }

        double inv_det = 1 / det;

        Vec3 tvec = ray.Origin() - a.geom_vertex.point;
        double u = tvec.Dot(pvec) * inv_det;
        if (u < 0 || u > 1) {
            return {};
        }

        Vec3 qvec = tvec.Cross(ab);
        double v = ray.Direction().Dot(qvec) * inv_det;
        if (v < 0 || u + v > 1) {
            return {};
        }

        double t = ac.Dot(qvec) * inv_det;
        if (t < 1e-6) {
            return {};
        }
        return ray.Origin() + t * ray.Direction();
    }

    Vec3<double> Normal(const Vec3<double>& point) const {
        if (!a.vertex_normal) {
            auto result = (a.geom_vertex.point - b.geom_vertex.point)
                              .Cross(a.geom_vertex.point - c.geom_vertex.point);
            result.Normalize();
            return result;
        }
        // looking for barycentric coords
        Vec3 v0 = b.geom_vertex.point - a.geom_vertex.point;
        Vec3 v1 = c.geom_vertex.point - a.geom_vertex.point;
        Vec3 v2 = point - a.geom_vertex.point;
        // Cramer's rule
        double d00 = v0.Dot(v0);
        double d01 = v0.Dot(v1);
        double d11 = v1.Dot(v1);
        double d20 = v2.Dot(v0);
        double d21 = v2.Dot(v1);

        double denom = d00 * d11 - d01 * d01;

        double v = (d11 * d20 - d01 * d21) / denom;
        double w = (d00 * d21 - d01 * d20) / denom;

        return (1 - v - w) * a.vertex_normal->value + v * b.vertex_normal->value +
               w * c.vertex_normal->value;
    }
};

struct Sphere final : public Shape {
    Vec3<double> center;
    double radius;

    Sphere(const Vec3<double>& center, double radius, const std::string& material)
        : Shape(material), center(center), radius(radius) {
    }

    std::optional<Vec3<double>> Intersection(const Ray& ray) const {
        Vec3<double> new_center = center - ray.Origin();
        double t_ca = new_center.Dot(ray.Direction());
        if (t_ca < 0) {
            return {};
        }
        double d_square = new_center.Norm() - t_ca * t_ca;
        if (d_square > radius * radius) {
            return {};
        }
        double t_hc = sqrt(radius * radius - d_square);
        double t0 = t_ca - t_hc, t1 = t_ca + t_hc;

        if (t0 > t1) {
            std::swap(t0, t1);
        }

        if (t0 < 0 && t1 < 0) {
            return {};
        } else if (t0 < 0) {
            t0 = t1;
        }

        if (t0 < 1e-6) {
            return ray.Origin() + t1 * ray.Direction();
        }
        return ray.Origin() + t0 * ray.Direction();
    }

    Vec3<double> Normal(const Vec3<double>& point) const {
        auto result = point - center;
        result.Normalize();
        return result;
    }
};

struct LightPoint {
    Vec3<double> point;
    Vec3<double> color;
};

struct Scene {
    std::unordered_map<std::string, Material> materials;
    std::vector<Triangle> triangles;
    std::vector<Sphere> spheres;
    std::vector<LightPoint> lights;
};

void ReadMaterialFile(const std::string& filename,
                      std::unordered_map<std::string, Material>* materials) {
    std::ifstream mtl_file;
    mtl_file.open(filename);
    std::string line;
    Material current_material;
    std::string material_name;
    while (std::getline(mtl_file, line)) {
        line = Trim(line);
        if (line.empty() || line[0] == '#') {
            continue;
        }
        if (IsPrefix("newmtl", line)) {
            if (!material_name.empty()) {
                (*materials)[material_name] = current_material;
            }
            material_name = Trim(line.substr(6));
            current_material = Material();
        } else if (IsPrefix("illum", line)) {
            int value = std::stoi(Trim(line.substr(5)));
            current_material.is_illuminating = value > 2;
        } else if (line[0] == 'd') {
            double value = std::stod(Trim(line.substr(1)));
            current_material.transparency = 1 - value;
        } else if (IsPrefix("Tr", line)) {
            double value = std::stod(Trim(line.substr(2)));
            current_material.transparency = value;
        } else if (IsPrefix("Ns", line)) {
            double value = std::stod(Trim(line.substr(2)));
            current_material.specular_exponent = value;
        } else if (IsPrefix("Ni", line)) {
            double value = std::stod(Trim(line.substr(2)));
            current_material.refraction_index = value;
        } else if (IsPrefix("Ka", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            current_material.ambient = Vec3(values);
        } else if (IsPrefix("Ke", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            current_material.ambient_additional = Vec3(values);
        } else if (IsPrefix("Kd", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            current_material.diffuse = Vec3(values);
        } else if (IsPrefix("Ks", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            current_material.specular = Vec3(values);
        }
    }
    if (!material_name.empty()) {
        (*materials)[material_name] = current_material;
    }
    mtl_file.close();
}

Scene ReadSceneFile(const std::string& filename) {
    Scene scene;

    std::ifstream obj_file;
    obj_file.open(filename);
    std::string line;

    std::vector<std::string> material_libs;
    std::vector<GeometricVertex> geom_vertices;
    std::vector<TextureCoordinates> texture_coords;
    std::vector<VertexNormal> vertex_normals;
    std::string current_material;
    while (std::getline(obj_file, line)) {
        line = Trim(line);
        if (line.empty() || line[0] == '#') {
            continue;
        }
        if (IsPrefix("mtllib", line)) {
            std::string filename = Trim(line.substr(6));
            material_libs.push_back(filename);
        } else if (IsPrefix("usemtl", line)) {
            std::string material_name = Trim(line.substr(6));
            current_material = material_name;
        } else if (line.size() > 1 && line[0] == 'v' && isspace(line[1])) {
            std::vector values = SplitAndParse(Trim(line.substr(1)));
            double weight = 1;
            if (values.size() > 3) {
                weight = values.back();
                values.pop_back();
            }
            geom_vertices.push_back({Vec3(values), weight});
        } else if (IsPrefix("vt", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            while (values.size() < 3) {
                values.push_back(0);
            }
            texture_coords.push_back({Vec3(values)});
        } else if (IsPrefix("vn", line)) {
            std::vector values = SplitAndParse(Trim(line.substr(2)));
            vertex_normals.push_back({Vec3(values)});
        } else if (line[0] == 'P') {
            std::vector values = SplitAndParse(Trim(line.substr(1)));
            scene.lights.push_back({Vec3<double>(values[0], values[1], values[2]),
                                    Vec3<double>(values[3], values[4], values[5])});
        } else if (line[0] == 'S') {
            std::vector values = SplitAndParse(Trim(line.substr(1)));
            double radius = values.back();
            values.pop_back();
            scene.spheres.push_back({Vec3(values), radius, current_material});
        } else if (line[0] == 'f') {
            std::vector<Vertex> vertices;
            for (const std::string& vertex : Split(Trim(line.substr(1)), kSpaces)) {
                std::vector<std::string> indices = Split(vertex, "/", false);
                while (indices.size() < 3) {
                    indices.push_back("");
                }
                ssize_t geom_vertex_idx = ParseIndex(indices[0], geom_vertices.size());
                ssize_t texture_coord_idx = ParseIndex(indices[1], texture_coords.size());
                ssize_t vertex_normal_idx = ParseIndex(indices[2], vertex_normals.size());

                vertices.push_back({geom_vertices[geom_vertex_idx], {}, {}});
                if (texture_coord_idx >= 0) {
                    vertices.back().texture_coord = texture_coords[texture_coord_idx];
                }
                if (vertex_normal_idx >= 0) {
                    vertices.back().vertex_normal = vertex_normals[vertex_normal_idx];
                }
            }
            for (size_t i = 1; i + 1 < vertices.size(); ++i) {
                scene.triangles.push_back(
                    {vertices[0], vertices[i], vertices[i + 1], current_material});
            }
        }
    }
    obj_file.close();

    for (const auto& lib_name : material_libs) {
        std::string directory = filename.substr(0, filename.find_last_of("/") + 1);
        std::string full_lib_path = directory + lib_name;
        ReadMaterialFile(full_lib_path, &scene.materials);
    }
    return scene;
}

std::pair<Vec3<double>, const Shape*> FindIntersection(const Ray& ray, const Scene& scene) {
    Vec3<double> result(0);
    const Shape* shape = nullptr;
    for (const auto& sphere : scene.spheres) {
        auto inter_point = sphere.Intersection(ray);
        if (inter_point && (!shape || (result - ray.Origin()).Norm() >
                                          (inter_point.value() - ray.Origin()).Norm())) {
            result = inter_point.value();
            shape = &sphere;
        }
    }
    for (const auto& triangle : scene.triangles) {
        auto inter_point = triangle.Intersection(ray);
        if (inter_point && (!shape || (result - ray.Origin()).Norm() >
                                          (inter_point.value() - ray.Origin()).Norm())) {
            result = inter_point.value();
            shape = &triangle;
        }
    }
    return {result, shape};
}

Vec3<double> Diffuse(const Vec3<double>& point, const Shape* point_shape,
                     const Vec3<double>& normal, const Scene& scene) {
    Vec3<double> result(0);
    for (const auto& light : scene.lights) {
        Ray ray{light.point, point - light.point};
        auto [hit_point, shape] = FindIntersection(ray, scene);
        double light_dist = (light.point - point).Norm();
        if (shape && light_dist > (light.point - hit_point).Norm() &&
            (shape != point_shape || (hit_point - point).Norm() > 1e6)) {
            continue;
        }
        result += light.color * std::max(0.0, normal.Dot(ray.Direction()));
    }
    return result;
}

Vec3<double> Specular(const Ray& view_ray, const Vec3<double>& point, const Shape* point_shape,
                      double specular_exp, const Vec3<double>& normal, const Scene& scene) {
    Vec3<double> result(0);
    for (const auto& light : scene.lights) {
        Ray light_ray{light.point, point - light.point};
        auto [hit_point, shape] = FindIntersection(light_ray, scene);
        double light_dist = (light.point - point).Norm();
        if (shape && light_dist > (light.point - hit_point).Norm() &&
            (shape != point_shape || (hit_point - point).Norm() > 1e6)) {
            continue;
        }
        Vec3<double> v_r = 2 * normal.Dot(light_ray.Direction()) * normal - light_ray.Direction();
        result +=
            light.color * std::pow(std::max(0.0, view_ray.Direction().Dot(v_r)), specular_exp);
    }
    return result;
}

Vec3<double> GetColor(const Ray& ray, const Scene& scene, int depth, bool is_inside = false) {
    if (depth <= 0) {
        return Vec3<double>(0);
    }
    auto [point, shape] = FindIntersection(ray, scene);
    Vec3<double> result(0);
    if (shape && scene.materials.find(shape->material) != scene.materials.end()) {
        Material material = scene.materials.find(shape->material)->second;
        Vec3<double> normal = shape->Normal(point);
        if (normal.Dot(ray.Direction()) < 0) {
            normal *= -1;
        }
        if (!is_inside) {
            result = material.ambient + material.ambient_additional;
            result += material.diffuse * Diffuse(point, shape, normal, scene);
            result += material.specular *
                      Specular(ray, point, shape, material.specular_exponent, normal, scene);
        }
        if (material.is_illuminating) {
            if (is_inside || material.transparency) {
                double transparency = material.transparency;
                double refraction_index = material.refraction_index;
                if (is_inside) {
                    transparency = 1;
                    refraction_index = 1 / refraction_index;
                }
                auto refracted_ray = Refract(ray, -normal, point, refraction_index);
                if (refracted_ray) {
                    bool will_be_inside = !is_inside && typeid(*shape) == typeid(Sphere);
                    result += transparency *
                              GetColor(refracted_ray.value(), scene, depth - 1, will_be_inside);
                }
            }
            if (!is_inside) {
                Ray reflected_ray = Reflect(ray, normal, point);
                Vec3<double> reflect_color = GetColor(reflected_ray, scene, depth - 1, is_inside);
                result += material.diffuse * reflect_color *
                          std::max(0.0, normal.Dot(-reflected_ray.Direction()));
                result += material.specular * reflect_color;
            }
        }
    }
    return result;
}