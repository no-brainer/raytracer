#pragma once

#include <cmath>
#include <vector>

template <class T>
class Vec3 {
public:
    Vec3(T x, T y, T z) : x_(x), y_(y), z_(z) {
    }
    Vec3() : x_(T()), y_(T()), z_(T()) {
    }
    explicit Vec3(T scalar) : x_(scalar), y_(scalar), z_(scalar) {
    }
    explicit Vec3(const std::vector<T>& v) : x_(v[0]), y_(v[1]), z_(v[2]) {
    }
    explicit Vec3(const std::array<T, 3>& v) : x_(v[0]), y_(v[1]), z_(v[2]) {
    }

    Vec3 operator+(const Vec3& rhs) const {
        return Vec3(x_ + rhs.x_, y_ + rhs.y_, z_ + rhs.z_);
    }
    Vec3& operator+=(const Vec3& rhs) {
        return (*this) = (*this) + rhs;
    }
    Vec3 operator-() const {
        return Vec3(-x_, -y_, -z_);
    }
    Vec3 operator-(const Vec3& rhs) const {
        return Vec3(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
    }
    Vec3 operator-=(const Vec3& rhs) {
        return (*this) = (*this) - rhs;
    }
    Vec3 operator*(const Vec3& rhs) const {
        return Vec3(x_ * rhs.x_, y_ * rhs.y_, z_ * rhs.z_);
    }
    Vec3& operator*=(const Vec3& rhs) const {
        return (*this) = (*this) * rhs;
    }
    Vec3 operator/(const Vec3& rhs) const {
        return Vec3(x_ / rhs.x_, y_ / rhs.y_, z_ / rhs.z_);
    }
    Vec3& operator/=(const Vec3& rhs) const {
        return (*this) = (*this) / rhs;
    }

    Vec3 operator+(T scalar) const {
        return (*this) + Vec3(scalar);
    }
    Vec3& operator+=(T scalar) {
        return (*this) = (*this) + scalar;
    }
    Vec3 operator*(T scalar) const {
        return Vec3(x_ * scalar, y_ * scalar, z_ * scalar);
    }
    Vec3& operator*=(T scalar) {
        return (*this) = (*this) * scalar;
    }
    Vec3 operator/(T scalar) const {
        return Vec3(x_ / scalar, y_ / scalar, z_ / scalar);
    }
    Vec3& operator/=(T scalar) {
        return (*this) = (*this) / scalar;
    }
    Vec3 operator-(T scalar) const {
        return Vec3(x_ - scalar, y_ - scalar, z_ - scalar);
    }
    Vec3& operator-=(T scalar) {
        return (*this) = (*this) - scalar;
    }
    auto operator^(double power) const {
        return Vec3(std::pow(x_, power), std::pow(y_, power), std::pow(z_, power));
    }

    T Dot(const Vec3& rhs) const {
        return x_ * rhs.x_ + y_ * rhs.y_ + z_ * rhs.z_;
    }
    T Norm() const {
        return this->Dot(*this);
    }
    Vec3<T> Cross(const Vec3<T>& rhs) const {
        return Vec3<T>(y_ * rhs.z_ - z_ * rhs.y_, z_ * rhs.x_ - x_ * rhs.z_,
                       x_ * rhs.y_ - y_ * rhs.x_);
    }
    void Normalize() {
        T length = sqrt(this->Norm());
        x_ /= length;
        y_ /= length;
        z_ /= length;
    }

    Vec3<T> Transform(const std::vector<Vec3<double>>& matrix) const {
        return matrix[0] * x_ + matrix[1] * y_ + matrix[2] * z_;
    }

    // Getters
    T X() const {
        return x_;
    }
    T& X() {
        return x_;
    }
    T Y() const {
        return y_;
    }
    T& Y() {
        return y_;
    }
    T Z() const {
        return z_;
    }
    T& Z() {
        return z_;
    }

    // Boolean
    bool operator==(const Vec3<T>& rhs) const {
        return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
    }
    bool operator!=(const Vec3<T>& rhs) const {
        return !(*this == rhs);
    }

private:
    T x_;
    T y_;
    T z_;
};

template <class T>
Vec3<T> operator*(T scalar, const Vec3<T>& vec) {
    return vec * scalar;
}

template <class T>
Vec3<T> operator+(T scalar, const Vec3<T>& vec) {
    return vec + scalar;
}

template <>
Vec3<double>::Vec3() : Vec3(0.0) {
}