#pragma once

#include <cctype>
#include <string>
#include <vector>

const std::string kSpaces = " \t\n\v\f\r";

std::string Trim(const std::string& s) {
    size_t first_nonspace = s.find_first_not_of(kSpaces);
    if (first_nonspace == std::string::npos) {
        return s;
    }
    size_t last_nonspace = s.find_last_not_of(kSpaces);
    return s.substr(first_nonspace, last_nonspace - first_nonspace + 1);
}

bool IsPrefix(const std::string& prefix, const std::string& str) {
    auto res = std::mismatch(prefix.begin(), prefix.end(), str.begin());
    return res.first == prefix.end();
}

std::vector<double> SplitAndParse(const std::string& s) {
    std::vector<double> result;
    size_t idx = 0;
    std::string current_value;
    while (idx < s.size()) {
        if (isspace(s[idx])) {
            if (current_value.length()) {
                result.push_back(stod(current_value));
            }
            current_value = "";
        } else {
            current_value += s[idx];
        }
        ++idx;
    }
    if (current_value.length()) {
        result.push_back(stod(current_value));
    }
    return result;
}

std::vector<std::string> Split(const std::string& string, const std::string& delimiters,
                               bool skip_empty = true) {
    size_t start = 0;
    std::vector<std::string> result;
    while (1) {
        size_t end = string.find_first_of(delimiters, start);
        if (end == std::string::npos) {
            result.push_back(string.substr(start));
            break;
        }
        std::string substr = string.substr(start, end - start);
        if (!substr.empty() || !skip_empty) {
            result.push_back(substr);
        }
        start = end + 1;
    }
    return result;
}

ssize_t ParseIndex(const std::string& s, size_t container_size) {
    if (s.empty()) {
        return -1;
    }
    int index = std::stoi(s);
    if (index > 0) {
        --index;
    }
    index %= static_cast<int>(container_size);
    if (index < 0) {
        index += container_size;
    }
    return index;
}
